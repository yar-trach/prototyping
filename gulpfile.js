const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const runSequence = require('run-sequence');

require('./gulp-tasks/clean');
require('./gulp-tasks/fonts');
require('./gulp-tasks/images');
require('./gulp-tasks/lint');
require('./gulp-tasks/sass');
require('./gulp-tasks/useref');

// Sync changes with browser's live reloading
gulp.task('browserSync', () =>
  browserSync.init({
    server: {
      baseDir: 'app'
    }
  })
);

// Default process
gulp.task('default', (callback) => runSequence('sass', 'lint', 'browserSync', 'watch', callback));

// Build process
gulp.task('build', (callback) =>
  runSequence('clean:dist', ['sass', 'useref', 'images', 'fonts'], callback)
);

// Watch if SASS files added/changed
gulp.task('watch', () => {
  gulp.watch('app/scss/**/*.scss', ['sass', browserSync.reload]); // looking for changes in SASS files
  gulp.watch('app/**/*.html', browserSync.reload); // looking for changes in HTML files
  gulp.watch('app/js/**/*.js', ['lint', browserSync.reload]); // looking for changes in JS files
});
