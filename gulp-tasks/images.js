const gulp = require('gulp');
const cache = require('gulp-cache');
const imagemin = require('gulp-imagemin');

// Optimizing images
gulp.task('images', () =>
    gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)') // taking images
    .pipe(cache( // caching minified images locally
        imagemin() // minifying images
    ))
    .pipe(gulp.dest('dist/images')) // moving minified images to dist folder
);
