const gulp = require('gulp');

// Moving fonts
gulp.task('fonts', () =>
    gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
);
