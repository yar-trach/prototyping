const gulp = require('gulp');
const useref = require('gulp-useref');
const gulpIf = require('gulp-if');
const uglify = require('gulp-uglify');
const cssnano = require('gulp-cssnano');

// Optimizing CSS/JS files
gulp.task('useref', () =>
    gulp.src('app/*.html') // looking into HTML files in app folder
    .pipe(useref()) // concatenate files defined in HTML
    .pipe(gulpIf('*.js', uglify())) // minifies only if it's a JS file
    .pipe(gulpIf('*.css', cssnano())) // minifies only if it's a CSS file
    .pipe(gulp.dest('dist')) // move minified files to dist folder
);
