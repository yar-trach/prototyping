const gulp = require('gulp');
const del = require('del');
const cache = require('gulp-cache');

// Removing files
gulp.task('clean:dist', () => del.sync('dist'));
gulp.task('clean:cache', (callback) => cache.clearAll(callback));
