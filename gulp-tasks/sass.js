const gulp = require('gulp');
const sass = require('gulp-sass');
const changed = require('gulp-changed');
const autoprefixer = require('gulp-autoprefixer');

// Compiling SASS files into CSS
gulp.task('sass', () =>
    gulp.src([
        'app/scss/**/*.scss',
        '!app/scss/base/**/*.scss'
    ]) // source SASS files
    .pipe(sass().on('error', sass.logError))
    // .pipe(changed('app/.css/', { // use for next processes only updated file
    //     extension: '.css'
    // }))
    .pipe(sass()) // convert SASS to CSS
    .pipe(autoprefixer('last 2 versions')) // add prefixes to CSS properties
    .pipe(gulp.dest('app/.css')) // destination of converted CSS files
);
