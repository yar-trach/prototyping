# How to work with Prototyping

[![Section of Yar's School](./section_prototyping-lines.svg)](https://bitbucket.org/yar-trach/)

---

>Local static server will help you to develop static sites very quickly. To get this work we have to do a few steps:

```markdown
1. Create folder for new project and enter it:

    $ mkdir project_folder
    $ cd project_folder

2. Clone Prototyping into working folder:

    $ git clone https://yar-trach@bitbucket.org/yar-trach/prototyping.git .

3. Install all develop dependencies:

    $ npm Install
    # This command will install all modules needed for development.

4. Run server:

    $ gulp
    # Server will start at address http:localhost:3000. Follow comments in console.

5. Open project in text editor. It can be Atom/Sublime Text/Brackets or any other text editor/IDE you like.
```

## Structure of Prototyping
```
prototyping                 # Root folder
├─── .git                   # Git folder
├─── app                    # Main Application folder
|    ├─── .css              # Folder for temporary CSS files
|    ├─── fonts             # Folder with fonts
|    ├─── images            # Folder for images
|    ├─── js                # Folder for JS
|    ├─── prototype         # Folder for prototyping elements
|    |    ├─── pages        # Folder for prototyping pages
|    |    └─── partials     # Folder for prototyping elements
|    ├─── scss              # Folder for SASS files
|    └─── index.html        # Main HTML file
├─── gulp-tasks             # Folder for tasks for Task Runner Gulp
├─── node_modules           # Folder for Node Modules required for dev process
├─── .eslintrc              # File with configuration for linting of JS
├─── .gitignore             # File with ignored nodes of project
├─── gulpfile.js            # Main Task Runner file
├─── package-lock.json      # File with describing of exact tree generated when node_modules tree or package.json was changed
└─── package.json           # Main project file with configuration
```
